//
//  SplitViewController.swift
//  splitTest
//
//  Created by Jonas on 23/06/16.
//  Copyright © 2016 Jonas. All rights reserved.
//

import UIKit

class SplitViewController: UISplitViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSplitVc()
    }
    
    func setupSplitVc() {
        self.preferredDisplayMode = UISplitViewControllerDisplayMode.AllVisible
        self.maximumPrimaryColumnWidth = 100
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
